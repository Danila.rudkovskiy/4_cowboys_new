//scroll to
window.onscroll = function showHeader() {
	let header = document.querySelector('.header')

	if (window.pageYOffset > 50) {
		header.classList.add('header_sticky')
	} else {
		header.classList.remove('header_sticky')
	}
}

function scrollTo(element) {
	window.scroll({
		left: 0,
		top: element.offsetTop,
		behavior: 'smooth',
	})
}

/*var menu = document.querySelector('.menu')
var buttons = menu.querySelectorAll('a[href*="#"]')
var section = document.querySelector('.courses')

for (let button of buttons) {
	button.addEventListener('click', () => {
		scrollTo(section)
	})
}*/

//fixed header

let menu = document.querySelector('.menu')
let buttons = menu.querySelectorAll('a[href*="#"]')

for (let button of buttons) {
	button.addEventListener('click', function (e) {
		e.preventDefault()

		const blockID = button.getAttribute('href').substr(1)

		document.getElementById(blockID).scrollIntoView({
			behavior: 'smooth',
			block: 'start',
		})
	})
}

//slider
function SlidesPlugin(activeSlide = 1) {
	const slides = document.querySelectorAll('.slide')

	slides[activeSlide].classList.add('active')

	for (const slide of slides) {
		slide.addEventListener('click', () => {
			clearActiveClasses()
			slide.classList.add('active')
		})
	}

	function clearActiveClasses() {
		slides.forEach((slide) => {
			slide.classList.remove('active')
		})
	}
}
SlidesPlugin()

//modal Windows
const openModalButtons = document.querySelectorAll('[data-modal-target]')
const closeModalButtons = document.querySelectorAll('[data-close-button]')
const overlay = document.getElementById('overlay')

openModalButtons.forEach((button) => {
	button.addEventListener('click', () => {
		const modal = document.querySelector(button.dataset.modalTarget)
		openModal(modal)
	})
})

overlay.addEventListener('click', () => {
	const modals = document.querySelectorAll('.modal.active')
	modals.forEach((modal) => {
		closeModal(modal)
	})
})

closeModalButtons.forEach((button) => {
	button.addEventListener('click', () => {
		const modal = button.closest('.modal')
		closeModal(modal)
	})
})

function openModal(modal) {
	if (modal === null) return
	modal.classList.add('active')
	overlay.classList.add('active')
}

function closeModal(modal) {
	if (modal === null) return
	modal.classList.remove('active')
	overlay.classList.remove('active')
}

//active element of header
window.addEventListener('scroll', () => {
	let scrollDistance = window.scrollY
	console.log(scrollDistance)

	document.querySelectorAll('section').forEach((el, i) => {
		if (el.offsetTop - document.querySelector('.header').clientHeight <= scrollDistance) {
			document.querySelectorAll('.menu a').forEach((el, i) => {
				if (el.classList.contains('active')) {
					el.classList.remove('active')
				}
			})
			document.querySelectorAll('.nav li')[i].querySelector('a').classList.add('active')
		}
	})
})
