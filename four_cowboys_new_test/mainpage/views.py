from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import DetailView, View
from django.contrib.auth import authenticate, login
from .models import *


class MainPage(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'mainpage.html')
# Create your views here.
