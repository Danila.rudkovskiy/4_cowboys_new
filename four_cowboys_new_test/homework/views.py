from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import DetailView, View
from django.contrib.auth import authenticate, login
from .models import *
from .business_logic.homework_logic import *

class Homework(View):

    def get(self, request, *args, **kwargs):
        return render(request, 'personalAcc2.html', homework())


    def homework_list_count(request, pk):
        homework = Homework.objects.get(pk=pk)
        homework.save()
        return HttpResponseRedirect(homework.file.url)
