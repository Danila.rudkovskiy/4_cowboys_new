from django.db import models


class Homework(models.Model):
    '''Модель homework'''
    title = models.CharField(max_length=250, verbose_name='Заголовок')
    file = models.CharField(max_length=512, verbose_name='Файл')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создан')
    is_active = models.BooleanField(default=True, verbose_name='Модерация')
    updated = models.DateTimeField(auto_now=True, verbose_name='Изменен')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Домашняя работа'
        verbose_name_plural = 'Домашняя работа'
        ordering = ['-created']
# Create your models here.
