from django import forms
from django.contrib.auth.models import User


class UserRegistrationForm(forms.ModelForm):
    email = forms.EmailField(label="Email", required=False)
    surname = forms.CharField(label="Familiya",required=False)
    name = forms.CharField(label="imya",required=False)
    date = forms.DateField(label="date",required=False)
    password = forms.CharField(label="Password", widget=forms.PasswordInput,required=False)
    password2 = forms.CharField(label="Repeat Password", widget=forms.PasswordInput,required=False)

    class Meta:
        model = User
        fields = ['email', 'first_name','date', 'last_name', 'password','password2']

    def clean(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Password don`t match.')
        return cd


class LoginForm(forms.ModelForm):
    username = forms.CharField(label="Email")
    password = forms.CharField(label="Password",widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password']

    def clean(self):
        email = self.cleaned_data['username']
        password = self.cleaned_data['password']
        if not User.objects.filter(username=email).exists():
            raise forms.ValidationError(f'Пользователь с логином {email} не найден!!')
        user = User.objects.filter(username=email).first()
        if user:
            if not user.check_password(password):
                raise forms.ValidationError('Неверный пароль')

