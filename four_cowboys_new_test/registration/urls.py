from django.contrib import admin
from django.urls import path, include
from .views import *
urlpatterns = [
    path('registration/', UserRegistration.as_view(), name="registration_user"),
    path('login/', UserLogin.as_view(), name="login_user"),

]