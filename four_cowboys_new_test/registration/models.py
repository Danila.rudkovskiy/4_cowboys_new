from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Students(models.Model):
    slug = models.SlugField(unique=True)
    user = models.ForeignKey(User, verbose_name="Student", on_delete=models.CASCADE)#Удаление пользователя из системы удалит его slug
    date_birth = models.DateField(verbose_name="Date",default="")

    def ___str___(self):
        return self.name+ " " + self.lastname

# Create your models here.
