from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import DetailView, View
from django.contrib.auth import authenticate, login
from .models import *
from .forms import UserRegistrationForm, LoginForm


class UserRegistration(View):

    def get(self, request, *args, **kwargs):
        form = UserRegistrationForm(request.POST or None)
        context = {"form": form}
        return render(request, 'Registration.html', context)

    def post(self, request, *args, **kwargs):
        form = UserRegistrationForm(request.POST or None)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.username = form.cleaned_data['email']
            new_user.email = form.cleaned_data['email']
            new_user.first_name = form.cleaned_data['first_name']
            new_user.last_name = form.cleaned_data['last_name']
            print("new_user.last_name,new_user.first_name,new_user.email")
            new_user.save()
            new_user.set_password(form.cleaned_data['password'])
            new_user.save()
            Students.objects.create(slug=new_user, user=new_user)
            user = authenticate(username=form.cleaned_data['email'], password=form.cleaned_data['password'])
            login(request, user)
            return HttpResponseRedirect('/main_page')

        context = {'form': form}
        return render(request, 'Registration.html', context)


class UserLogin(View):

    def get(self, request, *args, **kwargs):
        form = LoginForm(request.POST or None)
        context = {"form": form}
        return render(request, 'Login.html', context)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST or None)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'],password=form.cleaned_data['password'])
            login(request, user)
            return HttpResponseRedirect('/main_page/')
        context = {"form": form}
        return render(request, 'Login.html', context)



