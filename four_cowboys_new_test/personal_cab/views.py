from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import DetailView, View
from django.contrib.auth import authenticate, login
from .models import *
from registration.models import *

class PersonalCab(View):

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            student = User.objects.get(id=request.user.id)
            context = {"students":student}
            return render(request, 'personalAcc.html',) #context)
        else:
            HttpResponseRedirect("login/")
