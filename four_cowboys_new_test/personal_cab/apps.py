from django.apps import AppConfig


class PersonalCabConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'personal_cab'
